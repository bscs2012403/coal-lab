.model small
.stack 100h
.data
msg db "university $"
.code
main proc   
    mov ax,@data
    mov ds,ax
    mov cx,10           ;cx,10 - for printing 10 times
    l:
    mov ah,09h
    int 21h
    loop l
    
  mov ah,4ch
  int 21h
main endp
end main