

.stack 100h
.model small
.data
v1 db '1'
v2 db ?
v3 db 0ah,0dh, "safwan $"
.code 
 main proc 
    mov ax,@data
    mov ds,ax
    mov dl,v1
    mov ah,02
    int 21h
    mov bl,v2
    mov bl,0ah,0dh,3 
    add bl,48
    mov dl,bl
    mov ah,02
    int 21h
    mov dl,offset v3
    mov ah,09
    int 21h

     end main
ret